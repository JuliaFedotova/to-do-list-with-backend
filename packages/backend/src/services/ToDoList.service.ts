import { CreateToDoListDto } from '../dto/CreateToDoListDto.dto';
import { Injectable } from '@nestjs/common';
import { List } from '../entities/toDoList.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UpdateNoteDto } from '../dto/UpdateNoteDto.dto';

@Injectable()
export class ToDoList {
    constructor(
        @InjectRepository(List)
        private readonly listRepository: Repository<List>,

    ){}
    async create(createToDoListDto: CreateToDoListDto) {
        await this.listRepository.save(createToDoListDto);

        return 'success';
    }

    async findAll() {
       return await this.listRepository.find();
    }

     async findOne(id: number) {
       return await this.listRepository.findOne({
            where: {
                id
            }
        });
    }

   async remove(id: number) {
       return  await this.listRepository.delete(id)
       // return `This action delete a #${id} note`
    }

   async update(id: number, updateNoteDto: UpdateNoteDto) {
       return await this.listRepository.update(id, updateNoteDto)
         // `This action update a #${id} note`
    }

}