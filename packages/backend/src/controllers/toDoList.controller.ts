import {Controller, Get, Query, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFile} from '@nestjs/common';
import { CreateToDoListDto } from '../dto/CreateToDoListDto.dto';
import { ToDoList } from '../services/ToDoList.service';
import {UpdateNoteDto} from "../dto/UpdateNoteDto.dto";
import {FileInterceptor} from "@nestjs/platform-express";


@Controller('todo-list')
export class toDoListController {

    constructor(private readonly toDoListService: ToDoList) {}

    @Post()
    create(@Body() createToDoListDto: CreateToDoListDto) {
        return this.toDoListService.create(createToDoListDto);
    }

    @Get()
    findAll() {
        return this.toDoListService.findAll();
    }

    @Get(':id')
    findOne(@Param('id') id: number) {
        return this.toDoListService.findOne(+id);
    }

    @Delete(':id')
    remove(@Param('id') id: number) {
        return this.toDoListService.remove(+id);
    }

    @Patch(':id')
    update(@Param('id') id: number, @Body() updateNoteDto: UpdateNoteDto ) {
        return this.toDoListService.update(+id, updateNoteDto);
    }

}
