import {
    Column,
    Entity,
    OneToMany,
    PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class List {
    @PrimaryGeneratedColumn({
        type: 'bigint',
    })
    id: number

    @Column()
    title: string;

}