import { Module } from '@nestjs/common';
import { AppController } from './controllers/app.controller';
import { AppService } from './services/app.service';
import {toDoListController} from "./controllers/toDoList.controller";
import {ToDoList} from "./services/ToDoList.service";
import {TypeOrmModule} from "@nestjs/typeorm";
import {List} from "./entities/toDoList.entity";

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'postgres',
            host: 'localhost',
            port: 5432,
            username: 'fedotova',
            password: '1q2w3e4r',
            database: 'todolist',
            synchronize: true,
            autoLoadEntities: true,
            entities: [List],
            cache: true,
        }),
        TypeOrmModule.forFeature([List])
    ],
    controllers: [AppController, toDoListController],
    providers: [AppService, ToDoList],
})
export class AppModule {}