import { PartialType } from '@nestjs/mapped-types';
import { CreateToDoListDto } from './CreateToDoListDto.dto';

export class UpdateNoteDto extends PartialType(CreateToDoListDto) {}