import {notesApi} from "@/api/notes.api";

export const api = {
    notes: notesApi,
}
