import type {Notes} from "@/hooks/useNotes";
import type {Note} from "backend/dist/interfaces/note.interface";
export const notesApi = {
    async getNotes() {
        const response = await fetch('http://localhost:3000/todo-list');

        return await response.json();
    },


    async addNotes(formData: { title: string }) {
        const response = await fetch('http://localhost:3000/todo-list', {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        })

        return await response.json();
    },

    async deleteNotes(notesId: number): Promise<Notes[]> {
        const response = await fetch(`http://localhost:3000/todo-list/${notesId}`, {
            method: 'DELETE',
        })

        const json = await response.json();

        if(!response.ok) {
            throw new Error(json.error);
        }

        return json;
    },

    async updateNote(notesId: number, title: string): Promise<Note> {
        const response = await fetch(`http://localhost:3000/todo-list/${notesId}`, {
            method: 'PATCH',
            body: JSON.stringify({
                title,
            }),
            headers: {
                "Content-Type": "application/json"
            }
        });

        return await response.json();
    }

}