import {ref} from 'vue';
import {api} from "@/api/api";

export interface Notes  {
    title: string;
    id: number;
}

const notesList = ref<Notes[]>([]);

export function useNotes() {
    async function loadNotes() {
        notesList.value = await api.notes.getNotes();
    }

    return {
        notesList,
        loadNotes,
    }
}
